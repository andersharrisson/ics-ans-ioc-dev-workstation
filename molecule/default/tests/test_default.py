import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ioc_dev_workstations')


def test_installed_packages(host):
    for package in ("xfce4-session", "xrdp"):
        assert host.package(package).is_installed


def test_phoebus_installed(host):
    cmd = host.run("/usr/local/bin/phoebus -help")
    assert cmd.rc == 0


def test_conda_installed(host):
    cmd = host.run('/opt/conda/bin/conda --version')
    assert cmd.rc == 0
    assert cmd.stdout.startswith('conda')


def test_node_red_installed(host):
    if host.ansible.get_variables()['ioc_dev_workstation_node_red']:
        cmd = host.run("node-red --help")
        assert cmd.rc == 0
        assert "Usage: node-red" in cmd.stdout


def test_epics_nfs_available(host):
    cmd = host.run('ls -d /epics* | wc -l')
    assert cmd.rc == 0
    assert cmd.stdout.startswith('3')


def test_pvvalidator(host):
    cmd = host.run("source /opt/conda/etc/profile.d/conda.sh && conda activate epics && pvValidator.py --version")
    assert cmd.succeeded
